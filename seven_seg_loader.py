import csv
import random
import numpy as np 

import network

data = []

def index(x):
    return x.astype(int)

def vectorize(x):
    v = np.zeros((10,1))
    v[x] = 1.0
    return v


with open('data/new_seven_seg.csv', mode='r') as file:
    dataset = csv.reader(file)
    
    for line in dataset:
        line = np.array(line)
        data.append( (np.reshape(line[:7].astype(np.float), (7,1)), index(line[7])) )
        
#print(data)
random_sample = data
train_set = random_sample[:10]
sgd_train_set = [(x, vectorize(y)) for (x,y) in random_sample[:2000]]
evaluate_set = random_sample[:]