# Nerual net
## Dependencies
- pickle
- gzip
- numpy 
- matplotlib


## Taula de paràmetrs

| Iterations    | Max Poblation | Mutation Chance   | Mutation Size | Select Best   | Score |
| :--------:    | :-----------: | :-------------:   | :-----------: | :---------:   | :--- :|
| 30            | 32            | 5                 | 2             | 4             | 2441  |
| 30            | 32            | 20                | 2             | 4             | 2877  |
| 30            | 32            | 15                | 4             | 4             | 2030  |

## Finals
Iris, [4,8,3], terations=30, max_poblation=32, mutation_chance=30, mutation_size=1, 70,20; 80,10; 90,1;