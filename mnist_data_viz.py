## Adaptat d'un repo de github random

import pickle
import gzip

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

def load_data():
    f = gzip.open('data/mnist.pkl.gz', 'rb')
    training_data, validation_data, test_data = pickle.load(f, encoding="latin1")
    f.close()
    return (training_data, validation_data, test_data)

def vectorize(x):
    v = np.zeros((10, 1))
    v[x] = 1.0
    return v


def load_data_wrapper():
    tr_d, va_d, te_d = load_data()
    training_inputs = [np.reshape(x, (784, 1)) for x in tr_d[0]] #Agafem cada imatge i la posem com a array
    #print(training_inputs[0])
    training_results = [vectorize(y) for y in tr_d[1]] #Agafem cada resultat i creem un array amb un 1 a la posició resultat i 0 a la resta
    #print(training_results[0])
    training_data = zip(training_inputs, training_results) #Fem el zip de les imatges i resultats
    validation_inputs = [np.reshape(x, (784, 1)) for x in va_d[0]]
    validation_data = zip(validation_inputs, va_d[1])
    test_inputs = [np.reshape(x, (784, 1)) for x in te_d[0]]
    test_data = zip(test_inputs, te_d[1])
    return (training_data, validation_data, test_data)

def load_genetic():
    tr_d, va_d, te_d = load_data()
    training_inputs = [np.reshape(x, (784, 1)) for x in tr_d[0]]
    train_set = zip(training_inputs[:300], tr_d[1][:300])
    evaluate_inputs = [np.reshape(x, (784, 1)) for x in te_d[0]]
    evaluate_set = zip(evaluate_inputs, te_d[1])

    return (list(train_set), list(evaluate_set))

#training_data, validation_data, test_data = load_data()
#
#
#train_x, train_y = training_data
#print(train_y[1])
#plt.imshow(train_x[1].reshape((28, 28)), cmap=cm.Greys_r)
#plt.show()