import csv
import random
import numpy as np 

import network

data = []

def index(x):
    if x == "g":
        return 0
    else:
        return 1

def vectorize(x):
    v = np.zeros((2,1))
    v[x] = 1.0
    return v


with open('data/ionosphere.csv', mode='r') as file:
    dataset = csv.reader(file)
    
    for line in dataset:
        line = np.array(line)
        data.append( (np.reshape(line[:34].astype(np.float), (34,1)), index(line[34])) )
        
#print(data)
random_sample = data
np.random.shuffle(random_sample)
train_set = random_sample[:175]
sgd_train_set = [(x, vectorize(y)) for (x,y) in train_set]
evaluate_set = random_sample[175:]