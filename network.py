import random
import numpy as np
import time

class Network(object):

    #Constructor
    def __init__( self, layer_dimensions, weights=None, biases=None):
        self.num_layers = len(layer_dimensions)
        self.layer_dimensions = layer_dimensions
        if biases == None:
            #Fiquem un valor aleatori a cada neurona
            self.biases = [np.random.randn(y, 1) for y in layer_dimensions[1:]] #Posem bias a tots excepte la input
        else:
            self.biases = biases
        if weights == None:
            #Inizialitzem els pesos de les arestes aleatoriament
            self.weights = [np.random.randn(y, x) for x, y in zip(layer_dimensions[:-1], layer_dimensions[1:])]
        else:
            #Inizialitzem els pesos de les arestes
            self.weights = weights

    # Donat un valor d'entrada fa tot el feedforward i dóna la sortida
    def compute(self, input_value):
        output_value = input_value
        for i in range(self.num_layers-1):
            output_value = sigmoid(np.dot(self.weights[i], output_value) + self.biases[i])
        return output_value
    
    def compute_custom_params(self, weights, biases, input_value):
        output_value = input_value
        for i in range(self.num_layers-1):
            output_value = sigmoid(np.dot(weights[i], output_value) + biases[i])
        return output_value

    def genetic_learning(self, test_data, evaluation_function, max_time=60, max_poblation=32, n_parents = 3, mutation_chance=2, mutation_size=5):
        test_data = list(test_data)
        poblation = self.generate_random_individuals(max_poblation,self.layer_dimensions)
        start_time = time.time()
        iteration = 0
        best = 0
        while(time.time()-start_time<max_time):
            iteration += 1
            new_poblation=[]
            evaluations=[]

            #Avaluacio
            for i in range(len(poblation)):
                evaluations.append((self.genetic_evaluate(poblation[i], evaluation_function, test_data), i))

            #Seleccio
            evaluations.sort(key=lambda x: x[0], reverse=True)
            #Mutacio i reproduccio
            for i in range(n_parents):
                x = evaluations[i][1]
                new_poblation.append(poblation[x])
                for j in  range(i+1,n_parents):
                    y = evaluations[j][1]
                    new_poblation.append(self.mutate(self.crossover(poblation[x], poblation[y]), mutation_chance, mutation_size))

            #Imprimim el resultat amb evaluate de la millor en aquesta generacio
            best = evaluations[0][0]
            print("Iteration", iteration, "completed, with evaluation of", self.genetic_evaluate(poblation[evaluations[0][1]],self.evaluate,test_data))
            poblation = new_poblation

        #Fem l'ultima avaluacio de la poblacio final
        evaluations=[]
        for i in range(len(poblation)):
            evaluations.append((self.genetic_evaluate(poblation[i], evaluation_function, test_data), i))
        best = max(evaluations, key=lambda x: x[0])

        print("Pseudo evaluation final value =", best[0])
        print("Total correct: ",  self.genetic_evaluate(poblation[best[1]],self.evaluate,test_data))

    
    #Evaluacio d'un individu amb la funcio pasada per parametre
    def genetic_evaluate(self,individual,evaluate,test_data):
        self.weights = individual[0]
        self.biases = individual[1]
        return evaluate(test_data)

    #Reproduccio entre dos individuals fent la mitja
    def crossover(self,parent1, parent2):
        child_weights = []
        for i in range(len(parent1[0])):
            child_weights.append((np.array(parent1[0][i]) + np.array(parent2[0][i])) / 2.0)
        child_bias = (np.array(parent1[1], dtype=object) + np.array(parent2[1], dtype=object)) / 2.0

        return (child_weights,child_bias)

    def mutate(self,individual, mutation_chance, mutation_size):
        weights, biases = individual
        for i in range(len(weights)):
            for j in range(len(weights[i])):
                for k in range(len(weights[i][j])):
                    if random.randint(0,100) <= mutation_chance:
                        weights[i][j][k] += 2*random.random()*mutation_size - mutation_size

        for i in range(len(biases)):
            for j in range(len(biases[i])):
                if random.randint(0,100) <= mutation_chance:
                    biases[i][j] += 2*random.random()*mutation_size - mutation_size
        
        return (weights,biases)

    def generate_random_individuals(self,number_individuals,layer_dimensions):
        poblation = []
        for i in range(number_individuals):
            biases = [np.random.randn(y, 1) for y in layer_dimensions[1:]]
            weights = [np.random.randn(y, x) for x, y in zip(layer_dimensions[:-1], layer_dimensions[1:])]
            poblation.append((weights, biases))
        return poblation
        
    def stochastic_gradient_descent(self, training_data, n_iterations, batch_size, learning_rate, test_data):

        training_data = list(training_data)
        test_data = list(test_data)
        n_data = len(training_data)
        n_test = len(test_data)

        for i in range(n_iterations):
            random.shuffle(training_data)
            #Dividim training_data en conjunts de mida batch_size
            random_batches = [training_data[k:k+batch_size] for k in range(0, n_data, batch_size)]
            for batch in random_batches:
                gradient_biases = [np.zeros(bias.shape) for bias in self.biases]
                gradient_weights = [np.zeros(weight.shape) for weight in self.weights]
        
                for input_value, desired_value in batch:
                    # Calculem com creix o decreix el error
                    delta_gradient_biases, delta_gradient_weights = self.backpropagation(input_value, desired_value)
                    for j in range(self.num_layers-1):
                        gradient_biases[j] += delta_gradient_biases[j]
                        gradient_weights[j] += delta_gradient_weights[j]
            
                for j in range(self.num_layers-1):
                    self.weights[j] -= (learning_rate/len(batch))*gradient_weights[j]
                    self.biases[j] -= (learning_rate/len(batch))*gradient_biases[j]
                    
            print("Iteration ",i, ": ", self.evaluate(test_data),"/", n_test)
                

    def backpropagation(self, input_value, desired_value):
        weighted_sums = [] #Array on guardem les sumes ponderades de cada capa
        activations = [] #Array on guardarem els calors d'activació (sigmoid(weighted_sum)) 
        activations.append(input_value)
        for i in range(self.num_layers-1):
            #print(self.weights[i].shape, activations[i].shape, self.biases[i].shape)
            weighted_sum = np.dot(self.weights[i], activations[i]) + self.biases[i]
            activation = sigmoid(weighted_sum)
            
            weighted_sums.append(weighted_sum)
            activations.append(activation)
        
        gradient_biases = [np.zeros(bias.shape) for bias in self.biases]
        gradient_weights = [np.zeros(weight.shape) for weight in self.weights]
        
        delta_output = self.cost_derivative(activations[-1], desired_value)*sigmoid_prime(weighted_sums[-1])
        
        gradient_biases[-1] = delta_output
        gradient_weights[-1] = np.dot(delta_output, activations[-2].transpose())
        
        delta_layer = delta_output
        
        for i in range(self.num_layers-1-1-1, -1, -1):
            delta_layer = np.dot(self.weights[i+1].transpose(), delta_layer)*sigmoid_prime(weighted_sums[i]) 
            gradient_biases[i] = delta_layer
            gradient_weights[i] = np.dot(delta_layer, activations[i].transpose()) #Activations té un valor menys que weights, per això l'index és i!
        
        return gradient_biases, gradient_weights

    def evaluate(self, test_data):
        test_results = []
        for x,y in test_data:
            index = np.argmax(self.compute(x))
            test_results.append((index, y))
        #test_results = [(np.argmax(self.compute(x)), y) for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)

    def evaluate_error_sum(self, test_data):
        test_results=[]
        total = 0
        for x,y in test_data:
            test_results = self.compute(x)
            if(np.argmax(test_results) == y):
                for i in range(len(test_results)):
                    if i == y:
                        total += 1
                    else:
                        total -= test_results[i]/90.0
        return total


    def evaluate_error_all(self, test_data):
        test_results=[]
        total = 0
        for x,y in test_data:
            test_results = self.compute(x)
            if(np.argmax(test_results) == y):
                for i in range(len(test_results)):
                    if i == y:
                        #total += test_results[i]
                        total += 1
                    else:
                        total -= test_results[i]/90.0
                        #total -= 0
            else:
                total -= (1 - test_results[y])


        return total


    def evaluate_custom_params(self, weights, biases, test_data):
        test_results = []
        for x,y in test_data:
            index = np.argmax(self.compute_custom_params(weights, biases, x))
            #print(index, y)
            test_results.append((index, y))
        #test_results = [(np.argmax(self.compute(x)), y) for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)
    
    def cost_derivative(self, predictions, real):
        return (predictions-real)


def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    return sigmoid(z)*(1-sigmoid(z))



#import mnist_data_viz
#net = Network([784,30,10])
#training_data, validation_data, test_data = mnist_data_viz.load_data_wrapper()
#net.stochastic_gradient_descent(training_data, 30, 10, 3, test_data)

