import network
import random
import numpy as np 
import matplotlib as mpl
import iris_loader
import mnist_data_viz
import copy


layer_dimensions = [4,8,3]
#weights = np.array([np.random.randn(y, x) for x, y in zip(layer_dimensions[:-1], layer_dimensions[1:])], dtype=object)
#biases = np.array([np.random.randn(y, 1) for y in layer_dimensions[1:]], dtype=object)
#
#print(weights[1])

def weights_to_vector(weights):
    weights = np.array(weights, dtype=object) #Ho convertim a np array per si de cas (les matrius de network no són nparray)
    v = []
    for i in range(len(weights)): #Per a cada matriu de transcissió entre capes:
        dim_x, dim_y = weights[i].shape #Agafem les seves dimension
        v.append(np.reshape(weights[i], dim_x*dim_y)) #La guardem en forma d'array
    v = np.concatenate([x for x in v]) #Transformem l'array d'arrays en un sol array.
    return v

def biases_to_vector(biases):
    v = []
    for bias in biases:
        v.append(np.reshape(bias, -1))
    v = np.concatenate(v)
    return v
        

def vector_to_weights(vector, layer_dimensions):
    vectors = []
    start_index = 0
    for i in range(len(layer_dimensions)-1): #Hi ha n_layers -1 matrius a recompondre
        end_index = start_index + layer_dimensions[i]*layer_dimensions[i+1] #Calculem l'index de l'últim element + 1 (els splits fan fins :b-1)
        vectors.append(np.reshape(vector[start_index:end_index], (layer_dimensions[i+1], layer_dimensions[i]))) #Agafem la porció de vector que ens interessa i la passem a matriu amb les mides corresponents
        start_index = end_index #Definim el nou index inicial (no li hem de sumar res perquè per fer l'split ja tenim l'últim índex +1)
    return vectors

def vector_to_biases(vector, layer_dimension):
    biases = []
    start_index = 0
    for i in range(1, len(layer_dimensions)):
        end_index = start_index + layer_dimensions[i]
        biases.append(np.reshape(vector[start_index:end_index], (layer_dimensions[i], 1)))
        start_index = end_index
    return biases


def crossover(i1, i2):
    i1 = list(i1)
    i2 = list(i2)
    split_index = random.randint(0, len(i1))
    return i1[:split_index] + i2[split_index:], i1[split_index:]+i2[:split_index]

def mutation(individual, rate):
    return [gene * random.randint(-5,5) if random.uniform(0, 1) <= rate else gene for gene in individual]

#print(weights)
#vec = weights_to_vector(weights)
#print(vec)
#mat = vector_to_weights(vec, [4,8,3])
#print(mat)
#    
#print(biases)
#vec = biases_to_vector(biases)
#print(vec)
#mat = vector_to_biases(vec, [4,8,3])
#print(mat)
    

#n_networks=20
#iterations = 50
#rate = 0.05
#best_evaluation = 0
#optimal_parameters = ()
#networks = [network.Network(layer_dimensions) for i in range(n_networks)]
#for i in range(iterations):
#    evaluation = [(net, net.evaluate(iris_loader.test_set)) for net in networks]
#    evaluation.sort(reverse=True, key=lambda x: x[1])
#    print(evaluation)
#    if evaluation[0][1] > best_evaluation:
#        best_evaluation = evaluation[0][1]
#        optimal_parameters = (evaluation[0][0].weights, evaluation[0][0].biases)
#    print("Best evaluation at iteration", i, best_evaluation, optimal_parameters)
#    
#    networks.clear()
#    for j in range(n_networks//2):
#        weights1 = weights_to_vector(evaluation[j][0].weights)
#        biases1 = biases_to_vector(evaluation[j][0].biases)
#        
#        rnd_mate = random.choice(evaluation)[0]
#        weights2 = weights_to_vector(rnd_mate.weights)
#        biases2 = biases_to_vector(rnd_mate.biases)
#        
#        
#        
#        w1, w2 = crossover(weights1, weights2)
#        b1, b2 = crossover(biases1, biases2)
#        
#        w1 = mutation(w1, rate)
#        w2 = mutation(w2, rate)
#        b1 = mutation(b1, rate)
#        b2 = mutation(b2, rate)
#        
#        w1 = vector_to_weights(w1, layer_dimensions)
#        b1 = vector_to_biases(b1, layer_dimensions)
#        w2 = vector_to_weights(w2, layer_dimensions)
#        b2 = vector_to_biases(b2, layer_dimensions)
#        
#        networks.append(network.Network(layer_dimensions, w1, b1))
#        networks.append(network.Network(layer_dimensions, w2, b2))
#    
#final = network.Network(layer_dimensions, optimal_parameters[0], optimal_parameters[1])
#print("Best individual: ", final.evaluate(iris_loader.mega_test_set), "/", len(iris_loader.mega_test_set))
    

layer_dimensions = [784,30,10]
training_data, validation_data, test_data = mnist_data_viz.load_data_wrapper()
training_data = list(training_data)
test_data = list(test_data)
n_networks=20
iterations = 10
rate = 0.5
best_evaluation = 0
optimal_parameters = ()
networks = [network.Network(layer_dimensions) for i in range(n_networks)]
print(networks[0].weights[0])
for i in range(iterations):
    evaluation=[]
    for net in networks:
        evaluation.append((net, net.evaluate(test_data)))
    evaluation.sort(reverse=True, key=lambda x: x[1])
    print([e[1] for e in evaluation])
    if evaluation[0][1] > best_evaluation:
        best_evaluation = evaluation[0][1]
        optimal_parameters = (evaluation[0][0].weights, evaluation[0][0].biases)
    print("Best evaluation at iteration", i, evaluation[0][1])
    
    networks.clear()
    for j in range(n_networks//2):
        weights1 = weights_to_vector(evaluation[j][0].weights)
        biases1 = biases_to_vector(evaluation[j][0].biases)
        
        rnd_mate = random.choice(evaluation)[0]
        weights2 = weights_to_vector(rnd_mate.weights)
        biases2 = biases_to_vector(rnd_mate.biases)
        
        
        
        w1, w2 = crossover(weights1, weights2)
        b1, b2 = crossover(biases1, biases2)
        
        w1 = mutation(w1, rate)
        w2 = mutation(w2, rate)
        b1 = mutation(b1, rate)
        b2 = mutation(b2, rate)
        
        w1 = vector_to_weights(w1, layer_dimensions)
        b1 = vector_to_biases(b1, layer_dimensions)
        w2 = vector_to_weights(w2, layer_dimensions)
        b2 = vector_to_biases(b2, layer_dimensions)
        
        networks.append(network.Network(layer_dimensions, w1, b1))
        networks.append(network.Network(layer_dimensions, w2, b2))

final = network.Network(layer_dimensions, optimal_parameters[0], optimal_parameters[1])
print("Best individual: ", final.evaluate(mnist_data_viz.load_data_wrapper()[2]), "/10000")
