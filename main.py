import mnist_data_viz
import ionosphere_loader
import iris_loader
import seven_seg_loader
import network

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import pickle
import random

print("Introduïu una de les següents opcions:")
print(" 0: Entrena una xarxa neuronal amb l'algorisme genètic i mostra l'evaluació (1 minut d'entrenament)")
print(" 1: Entrena una xarxa neuronal amb backpropagation i mostra l'evaluació (30 iteracions de SGD)")
print(" 2: Evalua la xarxa preentrenada per a la classificació de lliris")
print(" 3: Evalua la xarxa preentrenada per a la classificasió de paràmetres de radar")
print(" 4: Evalua la xarxa preentrenada per a la classificació de dígits expressats amb 7 segments")
option = int(input("Opció (0/1/2/3/4): "))

if(option == 0):
    test_data = list(ionosphere_loader.train_set)
    net = network.Network([34,15,5,2])
    net.genetic_learning(test_data=test_data,evaluation_function=net.evaluate_error_sum)
    evaluation_data = ionosphere_loader.evaluate_set
elif(option == 1):
    net = network.Network([784,30,10]) 
    training_data, validation_data, evaluation_data = mnist_data_viz.load_data_wrapper() #Carreguem els dataset
    net.stochastic_gradient_descent(training_data, 3, 10, 3, evaluation_data)
    evaluation_data = mnist_data_viz.load_genetic()[1]
elif(option == 2):
    infile = open('iris.net','rb')
    net = pickle.load(infile)
    infile.close()
    test_data = list(iris_loader.train_set)
    evaluation_data = list(iris_loader.evaluate_set)
elif(option == 3):
    infile = open('ionosphere.net','rb')
    net = pickle.load(infile)
    infile.close()
    test_data = list(ionosphere_loader.train_set)
    evaluation_data = list(ionosphere_loader.evaluate_set)
elif(option == 4):
    infile = open('7segments.net','rb')
    net = pickle.load(infile)
    infile.close()
    test_data = list(seven_seg_loader.train_set)
    evaluation_data = list(seven_seg_loader.evaluate_set)


print("Evaluation result:", net.evaluate(evaluation_data), "/", len(evaluation_data))
