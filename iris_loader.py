import csv
import random
import numpy as np 

import network

data = []

def index(x):
    if x == "Iris-setosa":
        return 0
    elif x == "Iris-versicolor":
        return 1
    elif x == "Iris-virginica":
        return 2
    else:
        return 3

def vectorize(x):
    v = np.zeros((3,1))
    v[x] = 1.0
    return v


with open('data/iris.csv', mode='r') as file:
    dataset = csv.reader(file)
    
    for line in dataset:
        line = np.array(line)
        data.append( (np.reshape(line[:4].astype(np.float), (4,1)), index(line[4])) )
        
#print(data)
#random_sample = random.sample(data, 20)

# random_sample = data
# np.random.shuffle(random_sample)
# train_set = [(x, vectorize(y)) for (x,y) in random_sample]
# test_set = data[:100]
# evaluate_set = data[100:]

#print(train_set[0][0].shape, train_set[0][1].shape)

#net = network.Network([4,8,3])
#net.stochastic_gradient_descent(train_set, 500, 10, 3, test_set)
#    

random_sample = data
np.random.shuffle(random_sample)
train_set = random_sample[:100]
sgd_train_set = [(x, vectorize(y)) for (x,y) in train_set]
evaluate_set = random_sample[100:]